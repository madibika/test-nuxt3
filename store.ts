import { defineStore } from "pinia";

export const useProductStore = defineStore("productStore", {
  state: () => ({
    products: [] as ProductsInfo[],
  }),
  actions: {
    async fetchProducts() {
      const { data }: any = await useFetch("/catalog/get/");
      if (data) {
        this.products = data;
      }
    },
  },
});

interface ProductsInfo {
  id: Number;
  name: String;
  price: String;
}
